import React, { Component } from 'react';
import {Typeahead} from 'react-bootstrap-typeahead'
import 'react-bootstrap-typeahead/css/Typeahead.css';
import 'react-bootstrap-typeahead/css/Token.css';

class App extends Component {


    constructor(props, context) {
        super(props, context);
        this.state = {
            options: [
                {id: 'an ID', label: 'a Label'}
            ],
            model: {
                fieldvalue: ""
            }

        }

    }

    updateAutocompleteFieldInmodel = (fieldName, v) => {
        this.updateModelState(fieldName, Array.isArray(v) ? (v.length > 0 ? (v[0].customOption === true ? v[0].label : v[0].id) : '') : v);
    };

    updateModelState = (field, value) => {
        let m = this.state.model;
        m[field] = value;
        this.setState({model: m});
    };

    render() {
    return (
      <div className="App">
          <div className="center-block">
              <div className="col-md-2 col-md-offset-5">
                  <br/>
                  <br/>
                  <br/>
                  <Typeahead
                      options={this.state.options}
                      onChange={(v) => this.updateAutocompleteFieldInmodel("fieldvalue", v)}
                      allowNew={true}
                      selected={[this.state.model.fieldvalue]}
                  />
              </div>
          </div>
      </div>
    );
  }
}

export default App;
